﻿namespace Ionic.Zip {

    public partial class ZipEntry {

        public short VersionMadeBy {
            get {
                return _VersionMadeBy;
            }
            set {
                _VersionMadeBy = value;
            }
        }
    }
}
    